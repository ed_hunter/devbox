# devbox

Local linux mint dev environment with docker and postgres pre-installed.

## Pre-requisites

* Download and install VirtualBox: https://www.virtualbox.org/wiki/Downloads
* **Disable Hyper-V for Windows**: https://developer.hashicorp.com/vagrant/docs/installation#windows-virtualbox-and-hyper-v
* Download and install vagrant: https://developer.hashicorp.com/vagrant/downloads
* Also install plugin Sync VirtualBox Guest Additions  
  https://www.serverlab.ca/tutorials/virtualization/how-to-auto-upgrade-virtualbox-guest-additions-with-vagrant/  
```vagrant plugin install vagrant-vbguest```

## Usage
All command should be executed from the current directory containing the _Vagrantfile_.  
* Create and configure the VM:  
```vagrant up```
* Suspend the VM:  
```vagrant suspend```
* Resume the VM:  
```vagrant resume```
* Remove VM:  
```vagrant destroy```
